<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use App\Models\Pengguna;

class PenggunaController extends Controller
{
    public function index(){
        return view('home');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'email'     => 'required',
            'password'  => 'required'
        ]);
        
        $user = Pengguna::where('email', $request->email)->first();
        
        $isValidUser = false;
        if ($user !== null && Hash::check($request->password, $user->password)) {
            $isValidUser = true;
        }
        
        if ($isValidUser) {
            return redirect()->route('pengguna.index');
        } else {
            return view('login');
        }
        
    }
}
